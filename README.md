# Mapa conceptual 1
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Computadoras Electrónicas <<rose>>
	* para la primera mitad del siglo 20 la población prácticamente se había duplicado <<your_style_name>>
		* en poco tiempo esos dispositivos electromecánicos que al principio eran de tamaño de gabinetes empezaron a crecer
			* hasta convertirse en monstruos gigantes que ocupaban toda una habitación
				* había un problema más como eran máquinas inmensas oscuras y calientes atraían insectos
				* también eran de costoso alto el mantenimiento
					* a ese punto era inevitable y necesaria una innovación
	* Harvard Mark I <<your_style_name>>
		* una de las computadoras electromecánicas más grandes construidas
			*_ construida por 
				*  IBM
					*_ en 
						* 1944 durante la segunda guerra mundial
			* uno delos usos que se le dio a esta máquina fue hacer simulaciones cálculos para el proyecto manhattan
				* es el proyecto que desarrolló la bomba atómica
		* caracteristicas
			* utilizaba un motor de 5 caballos de fuerza
			* 80 kilómetros de cable
			* tenía 765 mil componentes
			* un eje de 15 metros 
		* hacia tres sumas o restas por segundo la multiplicación llevaba seis segundos la división 15 
			* las operaciones complejas o las funciones trigonométricas llevan más de un minuto
		* lapiedra angular de estas bestias electromecánicas
			* era el relé
				* es un interruptor mecánico controlado eléctricamente
					* está compuesto por una bobina
						* el funcionamiento es el siguiente
							* cuando una corriente eléctrica atraviesa la bobina se genera un campo magnético el campo magnético atrae al brazo \nmetálico y luego como este brazo metálico está conectado a los contactos los une de esta manera cerrando el circuito
				* unbuen relé en 1940 
					* tenía la capacidad de cambiar de estados 150 veces por segundo
				* el problema con los relés es que
					* el brazo de hierro que se encarga de conectar el circuito tiene masa
						* significa que tenga masa que entra en juego la inercia
					* tenía aproximadamente 3500 reales
						* esto provocaba que por pura estadística fallas en un relé al día
	* en 1904 un físico inglés llamado john andrews fleming <<your_style_name>>
		* desarrolló un nuevo componente eléctrico llamado válvula térmica
			* está formada por
				* un filamento
				* dos electrodos que están situados dentro de un bulbo de cristal sellado
	* 1906 el inventor americano lee de forest <<your_style_name>>
		* agregó un tercer electrodo llamado electrodo de control
			* estos tubos tenían la capacidad de cambiar de estado miles de veces por segundo
				* para la década en 1940 se volvieron más accesibles
				*_ no eran perfectos
					* eran frágiles
					* se podían quemar como los focos
					* gran costosos
	* colossus <<your_style_name>>
		* fue instalada en el parque bridge lee en reino unido
			*_ craada para
				* ayudaba a decodificar las comunicaciones nazis 
		*_ diseñada por
			* el ingeniero tommy flowers en 1943
		*_ tenia
			* 1600 tubos de vacío
	* ENIAC <<your_style_name>>
		*_ realizaba
			* 5000 sumas y restas de 10 dígitos por segundo
		*_ fue
			* construidaen 1946 en la universidad de pensilvania
				*_ por
					* john mauchly y J. presper eckert
	* 1947 <<your_style_name>>
		* se creó el transistor
			* cumple la misma función que un relé o un tubo de vacío 
				*_ es decir 
					* un interruptor eléctrico
			*_ caracteristicas
				* podría cambiar de estados unas 10.000 veces por segundo
				* eran sólidos
			*_ por
				* los científicos del laboratorios bell
					* John bardeen, walter brattain
					* williams shokley
@endmindmap
```

# Mapa conceptual 2
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Arquitectura Von Neumann y Arquitectura Harvard <<green>>
	* ley de moore <<caf>>
		*_ en
			* la electrónica
				* el número de transistores por chip se duplica a cada año
					* cada 18 meses se duplica la potencia sin modificar el costo
					* eso hace que la potencia de procesamiento se puede ir incrementando
					* EL costo del chip no varia

			* performance
				*_ incrementa
					* la velocidad de procesador
					* la velocidad de memoria
				* la velocidad de memoria siempre corre por de la velocidad de procesador
		*_ establece 
			* que la velocidad procesado o la potencia de procesamiento de las computadas se duplica cada 12 meses
	* el funcionamiento de la computadora <<caf>>
		* antes
			*_ habia 
				* sistemas cableados
			*_ tenia
				* una entradade datos
				* una secuencia funciona y lógica aritmética
			* la programación era en cuanto a hardware
		* ahora
			* la programación ya se hace mediante software
			* le llebamos al procesador que implemente un montón de funciones
				*_ estas
					* están implementadas con combinación de compuertas logicas
						*_ que son 
							* secuencia de funciones lógica y aritmética, que se van a ejecutar
	* arquitectura de von neumann <<caf>>
		* modelo
			* los datos y los programas se almacenan en una única memoria lectura y escritura
				* los contenidos de memoria se acceden indicando la posición sin importar el tipo
			*_ Tiene
				* ejecución en secuencia 
				* representación binaria
		* una función
			* es un salto en la ejecución
				* porque
					* la función no la tengo insertada en el medio de mi programa
		* Este describe una arquitectura de diseño \npara una computadora digital electrónica
			* la separación de la memoria del CPU
				*_ genero un problema
					* llamada cuello de botella de Neumann
					* Esto se debe 
						* a que la cantidad de datos que pasa entre estos dos difiere muncho
			* interconexión
				* los componentes se comunican atreves de un sistema de buses
			* tiene tres partes fundamentales
				* la memoria principal
				* el módulo sistema de entrada y salida
				* CPU
					*_ que haya dentro
						* unidad de control
						* una unidad de aritmética lógica
					*_ tiene
						* registros 
							* DE buffer de entrada y salida
							* De registro dedirecciones de entrada y salida
							* de bufferde memoria
							* de contador del programa
							* de registro de instrucción
							* de dirección de memoria
		* surge el concepto de
			*_ programa almacenado
				* por el cual  se conosen las computadoras de este tipo
	* modelo de bus <<caf>>
		* su propósito es el de reducir lacantidad deinterconexión entre la CPU y el sistema
			* es un dispositivo en común entre dos o más dispositivos
				* existen varios tipos de buses que realiza la interconexión
					*_ ejemplo de buses
						* datos
						* control
						* dirección
			* si dos dispositivos trasmiten señal al mismo tiempo 
				* cada línea puede trasmitir señales que representan unos y ceros
				*_ esta
					*  puede distorsionarse y perder información
	* instrucciones <<caf>>
		* consisten en secuencias de 1 y 0
			* la CPU es quien se encarga de ejecutar dichas instrucciones
				* por ello emplean lenguajes
					*_ como
						* bajo nivel
							*_ ensamblador
						* alto nivel
							*_ python
							*_ java
	* ciclos <<caf>>
		* de una instrucción
			*_ fases
				* calculo la dirección de la instrucción
				* captación de la instrucción
				* decodificación de la instrucción
				* calculo de la dirección
				* captación del operando
				* operación con datos
				* calculo la dirección del operando
				* almacenamiento de los operando
		* de ejecución
			*_ fases
				* Fech
				* Decode
				* Execute
	*  Arquitectura Harvard <<caf>>
		* Originalmente se refiere a las arquitecturas de computadoras 
			*_ que 
				* utilizaban dispositivos de almacenamiento 
					* físicamente separados para las instrucciones y para los datos
			* Memorias
				* Fabricar memorias más rápidas tiene un precio muy alto
					*_ La solución
						* por tanto es proporcionar una pequeña cantidad de memoria muy rápida
							* conocida con el nombre de CACHE
						* La arquitectura Harvard ofrece una solución particular a este problema
							* que las instrucciones y datos se almacenen en CACHE separadas
								*_ la desventaja
									* es que dividen la memoria CACHE en 2
			* Este describe una arquitectura de \ndiseño para un computador digital eléctrico
				* partes que constan
					* Sistemas de entrada y salida
					* cpu
						* Dos unidades funcionales principales
							* Unidad aritmética y lógica
								* almacena el resultado en la memoria de datos
							* Unidad de control
								* lee la instrucción de la memoria de instrucciones
					* memoria principal
						*_ otras memorias
							* memoria de datos
								* Se almacenan los datos utilizados por los programas
								* Los datos varían continuamente por lo tanto utiliza memoria RAM
							* Memoria de instrucciones
								* Es la memoria donde se almacenan las instrucciones del programa que debe ejecutar el microcontrolador
									*_ Ejemplo de memorias
										* ROM
										* PROM
										* EPROM
										* EEPROM
									* Microcontroladores
										* Donde se utilizan
											* electrodomésticos
											* telecomunicaciones
											* automóviles
											* museos
@endmindmap
```

# Mapa conceptual 3
```plantuml
@startmindmap
<style>
mindmapDiagram {
  .green {
    BackgroundColor lightgreen
  }
  .rose {
    BackgroundColor #FFBBCC
  }
  .caf {
    BackgroundColor #FFCAA6
  }
  .your_style_name {
    BackgroundColor lightblue
  }
}
</style>
* Basura Electrónica <<your_style_name>>
	*_ los 
		*  elementos que extraemos de estos materiales <<green>>
			*_ son
				* platico
				* oro
				* cobalto
				* plata
				* cobre
			*_ es 
				* de los circuitos y hasta en los procesadores 
	*_ la
		* basura electrónica no es como cualquier otra basura <<green>>
			*_ la 
				* mayor parte de la basura electrónica
					* no termina en fábricas
		* unión europea <<green>>
			*_ implementó
				* una nueva ley en 2016
					*_ que
						* que requiere que cada país recoja 45 toneladas de residuos electrónicos por \ncada 100 toneladas de productos electrónicos puestos a la venta
	*_ a veces
		* no se reciclada de manera segura <<green>>
			*_ es
				* exportada en grandes contenedores a un país del tercer mundo
					* donde usan métodos muy peligrosos
						* para la extracion de los materiales
							* con impactos nefastos para el medio ambiente
					*_ ejemplo
						* china
						* méxico
						* india
	*_ porque
		* existe un mercado ilegal <<green>>
			*_ porque
				* reciclar los electrónicos de manera segura es un proceso costoso y complicadísimo
					* por eso 
						* usan métodos primitivos y mucho más baratos
			*_ riesgos
				* no tienen el equipo adecuado
					*_ entonces 
						* al derriten los electrónicos para extraer los recursos
							* están inhalando todos los vapores tóxicos
								*_ por lo que
									* sufren graves problemas de salud los empleados
	*_ son
		* todos los dispositivos electrónicos que ya cumplieron con su vida útil o se dañaron <<green>>
			*_ sus
				* desventajas
					* un equipo promedio tiene una vida útil de 3 a 5 años 
					* los residuos quemados y los químicos terminan en el río
					* los productos electrónicos están diseñados para durar \npoco obligando a los consumidores a reemplazarlos constantemente
					* contaminan el aire
					* contaminan el agua
					* 70% de las toxinas que se desprenden de los tiraderos de basura provienen de los desechos electrónicos 
				* ventajas
					* contamina menos reciclar elmineral que si se minara el mineral
					* hacen que nuestro quehacer diario sea menos complicado
	*_ esconden 
		* otro precioso elemento <<green>>
			* los datos
				* muchas veces los discos duros pueden ser reparados
					*_ con
						* fines criminalesestán buscando información personal 
							*_ que 
								* puedan usar para robar la identidad de una persona
	*_ es
		* importante depositarlos en empresas de reciclaje <<green>>
			*_ que 
				* sea confiable y responsable en el manejo de los desechos 
	*_ el
		* proceso de reciclado <<green>>
			*_ se
				* pueden reducir los costos de la construcción de nuevos sistemas
				* los componentes con frecuencia contienen materiales valiosos como para reclamar por derecho propio 
			*_ pasos
				* la recepción del material eléctrico 
					*_ juntarlo 
					*_ clasificarlo
				* desarmarlo y clasificarlo de acuerdo a sus componentes
				* la limpieza de todas las piezas
				* se almacena con su correspondiente clasificación 
				* se exporta para que la matriz realice el último trabajo de fundición
				* recuperación de los metales
	*_ se
		*  estima que uno de cada tres contenedores <<green>>
			*_ que
				* sale de la unión europea contiene residuos electrónicos 
					*_ de
						* forma ilegales
	*_ los
		* raee <<green>>
			*_ es
				* un complejo de metales plásticos y otros compuestos 
					*_ que
						* deben ser tratados adecuadamente
			*_ son
				* los residuos electrónicos y aparatos eléctricos
					*_ que
						* dejan de funcionar cuando ya pasa o culmina su vida útil
							*_ por ejemplo
								* una licuadora 
								* un televisor 
								* una computadora 
								* un celular
								* una proyector 
								* una laptop
								* una consola de videojuegos
@endmindmap
```
